function! s:insert_gates()
  let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
  execute "normal! i#ifndef " . gatename
  execute "normal! o#define " . gatename
  normal! 3o
  normal! Go#endif
  normal! kk
endfunction

if !filereadable(expand('%'))
  if expand('%:e') =~? '\v^h%(pp|h|\+\+|xx)?$'
    call <SID>insert_gates()
  endif
endif
