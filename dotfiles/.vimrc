if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim-plug
call plug#begin('~/.vim/plugged')
    " Basic installation
    Plug 'tpope/vim-sensible'         " Recommended base settings
    Plug 'tpope/vim-repeat'           " '.' now repeats all maps, not just base commands
    Plug 'tpope/vim-speeddating'      " Correct date increment
    Plug 'tpope/vim-vinegar'          " Better default file-manager
    Plug 'tpope/vim-endwise'          " Auto close new structures
    Plug 'tpope/vim-obsession'        " Tweaks to built-in :mksession
    Plug 'aymericbeaumet/vim-symlink' " Make vim resolves symlinks
    Plug 'sheerun/vim-polyglot'       " Syntax highlighting
    Plug 'chrisbra/Recover.vim'       " Compare current and swap file when recovering

    " Theme
    Plug 'catppuccin/vim', { 'as': 'catppuccin' } " Colorscheme
    Plug 'itchyny/lightline.vim'                  " Lightline
    Plug 'junegunn/rainbow_parentheses.vim'       " Rainbow parentheses
    Plug 'RRethy/vim-illuminate'                  " Highlight all instances of the word under the cursor
    Plug 'ryanoasis/vim-devicons'                 " Display glyphs correctly

    " Mappings
    Plug 'tpope/vim-commentary'            " Comment/uncomment lines
    Plug 'tpope/vim-rsi'                   " Readline mappings in insert/command mode
    Plug 'tpope/vim-unimpaired'            " Many mappings starting with '[' or ']'
    Plug 'tpope/vim-surround'              " Change/Delete surroundings (cs / ds)
    Plug 'tpope/vim-abolish'               " Subvert (substitions with plurals) and Coerce (cr)
    Plug 'machakann/vim-swap'              " Swap function arguments
    Plug 'nathanaelkane/vim-indent-guides' " Show indentation (<leader>ig)
    Plug 'AndrewRadev/splitjoin.vim'       " Split and merge multi-lines constructs (gS and gJ)
    Plug 'willchao612/vim-diagon'          " Drawing tables and graphs (https://arthursonzogni.com/Diagon/)

    " Math
    Plug 'glts/vim-magnum'     " Integer support (dependance of vim-radical)
    Plug 'glts/vim-radical'    " Change number basis (gA to show all bases, cr[dxob] to convert)
    Plug 'theniceboy/vim-calc' " Compute math expressions (<leader>c)

    " Copilot
    Plug 'github/copilot.vim'

    " Code formating
    Plug 'editorconfig/editorconfig-vim'
    Plug 'ntpeters/vim-better-whitespace'
    Plug 'vim-autoformat/vim-autoformat'

    " Tmux-Vim integration
    Plug 'roxma/vim-tmux-clipboard'
    Plug 'christoomey/vim-tmux-navigator'

    " Startify (recent files)
    Plug 'mhinz/vim-startify'

    " Man / Info viewers in vim
    Plug 'HiPhish/info.vim'
    Plug 'gauteh/vim-cppman'

    " Edit audio tags
    Plug 'AndrewRadev/id3.vim'

    " Undo graph
    Plug 'mbbill/undotree'

    " Alignement
    Plug 'godlygeek/tabular'

    " CtrlP
    Plug 'ctrlpvim/ctrlp.vim'

    " Motions
    Plug 'terryma/vim-expand-region'

    " Multiple Cursors
    Plug 'mg979/vim-visual-multi'

    " Git
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-rhubarb'
    Plug 'shumphrey/fugitive-gitlab.vim'
    Plug 'airblade/vim-gitgutter'

    " YouCompleteMe
    Plug 'ycm-core/YouCompleteMe', {'do': 'python3 install.py --all'}
    Plug 'rdnetto/YCM-Generator', {'branch': 'stable'}

    " Snippets
    Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

    " Tags
    Plug 'craigemery/vim-autotag'
    Plug 'preservim/tagbar'

    " LSP client for linting
    Plug 'dense-analysis/ale'

    " VimTex
    Plug 'lervag/vimtex'

    " Language specific
    Plug 'tmhedberg/SimpylFold'                 " Python: Fix folding
    Plug 'rust-lang/rust.vim'                   " Rust
    Plug 'https://codeberg.org/soli/prolog-vim' " Prolog
    Plug 'hura/vim-asymptote'                   " Asymptote
call plug#end()

" Set leader
let mapleader = "\<space>"
let maplocalleader = "\<space>"
noremap <Leader>a= :Tabularize /=<CR>
noremap <Leader>a+ :Tabularize /+<CR>
noremap <Leader>a# :Tabularize /#<CR>
noremap <Leader>a% :Tabularize /%<CR>
noremap <Leader>a& :Tabularize /&<CR>
noremap <Leader>a/ :Tabularize /\/<CR>
noremap <Leader>a: :Tabularize /:<CR>
noremap <Leader>a, :Tabularize /,<CR>
noremap <Leader>a" :Tabularize /"<CR>
noremap <Leader>at :Tabularize /\t<CR>
noremap <Leader>a<Tab> :Tabularize /\t<CR>
noremap <Leader>a<bar> :Tabularize /<bar><CR>
noremap <Leader>a<space> :Tabularize /<space><CR>
noremap <Leader>c :call Calc()<CR>
noremap <Leader>D :Diagon<Space>
noremap <Leader>af ix<ESC>x:undojoin \| Autoformat<CR>
noremap <leader>bt :tab ball<CR>
noremap <leader>q  :TagbarOpenAutoClose<CR>
noremap <leader>i :IndentGuidesToggle<CR>
noremap <leader>wr :WipeReg
noremap <leader>P vipJV"+yu
"               ^ Copie sur "+ le paragraphe courant en une ligne

imap <silent><script><expr> <C-J> copilot#Accept("\<CR>")
let g:copilot_no_tab_map = v:true

" Shortcuts
xnoremap gt :g/./Commentary<CR>:noh<CR>
nnoremap gj :SplitjoinSplit<CR>
nnoremap gk :SplitjoinJoin<CR>
nnoremap yog :GitGutterLineHighlightsToggle<CR>
nnoremap <F2> :UndotreeToggle<CR>
nnoremap <F4> :silent !Antidote -f %<CR>:redraw!<CR>
nnoremap <F5> :set list!<CR>
nnoremap <F6> :w !git diff % -<CR>:redraw!<CR>
inoremap <F7> <Esc>:silent exec '.!inkscape-figures create "'.getline('.').'" "'.b:vimtex.root.'/figures/"'<CR><CR>:w<CR>
nnoremap <F7> :silent exec '!inkscape-figures edit "'.b:vimtex.root.'/figures/" > /dev/null 2>&1 &'<CR><CR>:redraw!<CR>
nnoremap q: :q
nmap <C-s> <C-a>
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)
cnoremap w!! w !sudo tee > /dev/null %
command! WipeReg for i in range(65,122) | silent! call setreg(nr2char(i), []) | endfor

" Youcompleteme mappings and config
nnoremap <Leader>d :let g:ycm_show_diagnostics_ui=1 - g:ycm_show_diagnostics_ui<CR>:YcmRestartServer<CR>:e<CR>
nnoremap <leader>r :exec 'YcmCompleter RefactorRename' input( 'Rename to: ' ) \| tab ball<CR>
nnoremap <Leader>f :YcmCompleter FixIt<CR>
nnoremap <leader>gd :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>gD :YcmCompleter GoToDefinition<CR>
nnoremap <leader>gfw <Plug>(YCMFindSymbolInWorkspace)
nnoremap <leader>gfd <Plug>(YCMFindSymbolInDocument)

let g:ycm_global_ycm_extra_conf         = '~/.vim/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf            = 0
let g:ycm_use_clangd                    = 1
" let g:ycm_clangd_args                   = ['--clang-tidy']
let g:ycm_key_list_select_completion    = ['<c-n>', '<TAB>', '<Down>']
let g:ycm_key_list_previous_completion  = ['<c-p>', '<S-TAB>', '<Up>']
let g:ycm_key_list_stop_completion      = ['<C-y>']
let g:ycm_key_invoke_completion         = '<C-Space>'
let g:ycm_max_diagnostics_to_display    = 10000
let g:ycm_always_populate_location_list = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_add_preview_to_completeopt = 1

augroup MyYCMCustom
  autocmd!
  autocmd FileType * let b:ycm_hover = {
    \ 'command': 'GetDoc',
    \ 'syntax': &filetype,
    \ 'popup_params': {
    \     'maxwidth': 80,
    \     'border': [],
    \     'borderchars': ['─', '│', '─', '│', '┌', '┐', '┘', '└'],
    \   },
    \ }
augroup END

" ALE
let g:ale_linters_explicit = 1
let g:ale_linters = {
\   'python': ['flake8'],
\}
let g:ale_python_flake8_options = '--ignore=F403,F405,W503,W504'
let g:ale_fixers = {
\   'python': ['yapf'],
\   '*' : [],
\}
autocmd FileType python noremap <buffer> <Leader>f :ALEFix<CR>

" Python
let g:SimpylFold_docstring_preview = 1
let g:ycm_python_interpreter_path = ''
let g:ycm_python_sys_path = []
let g:ycm_extra_conf_vim_data = [
  \  'g:ycm_python_interpreter_path',
  \  'g:ycm_python_sys_path'
  \]

" vim-rsi tweaks
let g:rsi_no_meta = 1

" ctrlp / rg
let g:ctrlp_map = '<c-p>'
if executable('rg')
  set grepprg=rg\ --color=never
  let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'
  let g:ctrlp_use_caching = 0
else
  let g:ctrlp_clear_cache_on_exit = 0
endif

" undotree
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_DiffAutoOpen = 0
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile

" Git
let g:gitgutter_sign_removed = '-'
let g:gitgutter_preview_win_floating = 1
let g:github_user = 'jaja360'
let g:github_password = expand('$GITHUB_APIKEY')
let g:fugitive_gitlab_domains = ['https://gitlab.info.uqam.ca/']
let g:gitlab_api_keys = {'gitlab.com': expand('$GITLAB_APIKEY'), 'gitlab.info.uqam.ca': expand('$GITLAB_UQAM_APIKEY')}

" Format options
let g:better_whitespace_enabled = 1
let g:strip_whitelines_at_eof   = 1
let g:strip_whitespace_on_save  = 1
let g:strip_whitespace_confirm  = 0
let g:EditorConfig_exclude_patterns = ['fugitive://.*']
" let g:formatters_cpp            = ["clangformat"]

" Ultisnips tweaks
let g:UltiSnipsEditSplit           = 'tabdo'
let g:UltiSnipsExpandTrigger       = "<c-e>"
let g:UltiSnipsListSnippets        = "<c-tab>"
let g:UltiSnipsJumpForwardTrigger  = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsSnippetDirectories  = [$HOME.'/.vim/UltiSnips']
autocmd FileType snippets :DisableStripWhitespaceOnSave

" VimTex tweaks
set conceallevel=0
let g:tex_flavor              = 'latex'
let g:vimtex_view_method      = 'zathura'
let g:vimtex_quickfix_mode    = 0
" if empty(v:servername) && exists('*remote_startserver')
"     call remote_startserver('VIM')
" endif

" Vim-Vinegar
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+' " Hidden files by default

" Indent guides
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=darkgrey
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=grey

" Syntax highlighting
syntax enable
autocmd Syntax * syntax keyword Todo TEST containedin=.*Comment contained

" Fix colors when using kitty
source ~/.vim/kitty.vim

" Colorscheme
set termguicolors
colorscheme catppuccin_mocha
let g:lightline = {'colorscheme': 'catppuccin_mocha'}
hi Search          ctermfg=Red ctermbg=Yellow
hi GitGutterAdd    ctermfg=2   ctermbg=0
hi GitGutterChange ctermfg=3   ctermbg=0
hi GitGutterDelete ctermfg=1   ctermbg=0
hi Conceal ctermbg=NONE

" Indentation
set tabstop=2 | set sw=2 | set et
au FileType make setl noexpandtab
au FileType python setl et sw=4 sts=4 ts=4

" Folding
set foldmethod=syntax
set foldlevelstart=99

" Display
set laststatus=2 " Always display the statusline in all windows
set showtabline=2 " Always display the tabline, even if there is only one tab
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)

" Spelling
set spelllang=en,fr
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" Misc options
if &modifiable
    set fileformat=unix
    set fileencoding=utf-8
endif
set encoding=utf-8
set formatoptions=tcroql
set mouse=a
set number
set autochdir
set hlsearch
set ignorecase
set smartcase
set lazyredraw
set autoindent
set smarttab
set textwidth=80
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set wildignore+=*/.git/*,*/tmp/*,*.swp
set tags=./tags;,tags;
set matchpairs+=<:>
packadd! matchit

" Enable RainbowParentheses
augroup rainbow_paren
  autocmd!
  autocmd FileType * RainbowParentheses
augroup END

" Source the vimrc file after saving it
autocmd! bufwritepost .vimrc source %
