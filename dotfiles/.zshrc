# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export ZSH=~/.oh-my-zsh

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# CASE_SENSITIVE="true"
# DISABLE_AUTO_UPDATE="true"

plugins=(command-not-found fancy-ctrl-z fzf npm emoji tmux sudo transfer
         kitty web-search pip python gitignore kubectl k9s fluxcd helm
         zsh-vi-mode zsh-autosuggestions fast-syntax-highlighting)

fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src
source "$ZSH/oh-my-zsh.sh"

# Allows you to type Bash style comments on your command line
setopt interactivecomments
setopt RM_STAR_WAIT

# --- Alias ---
if [ -x "$(command -v eza)" ]; then
  alias ls="eza --icons -g -h -H --git"
fi
if [ -x "$(command -v fastfetch)" ]; then
  alias neofetch="fastfetch"
fi
alias ll="ls -l"
alias la="ls -l -a"
alias rgsed="find | sad"
alias mutt="neomutt"

if [ -x "$(command -v bat)" ]; then
  export MANPAGER="sh -c 'col -bx | bat -l man -p'"
  export MANROFFOPT="-c"
  alias cat='bat'
fi

alias perfc="perf stat -e cpu-clock,cycles,instructions,branches,branch-misses,cache-references,cache-misses,LLC-loads,LLC-load-misses,LLC-stores,LLC-store-misses"
alias perfoc="perforator -e cpu-clock,cpu-cycles,instructions,branch-instructions,branch-misses,cache-references,cache-misses,ll-read-accesses,ll-read-misses,ll-write-accesses,ll-write-misses"
function memrank () {
  ps -eo size,pid,user,command --sort -size | awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }'
}
function viminfo () {
    vim -R -M -c "Info $1 $2" +only
}
alias info=viminfo
function ranger-cd () {
    tempfile="$(mktemp -t tmp.XXXXXX)"
    ranger --choosedir="$tempfile" "${@:-$(pwd)}"
    test -f "$tempfile" &&
    if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
        cd -- "$(cat "$tempfile")"
    fi
    rm -f -- "$tempfile"
}
alias ranger='ranger-cd'
bindkey -s "^o" "ranger\n" # Ctrl-o launches ranger

# Completion
fpath+=~/.zfunc
autoload -Uz compinit
compinit
zmodload -i zsh/complist
zstyle ':completion:*' menu select
setopt hash_list_all     # hash everything before completion
setopt completealiases   # complete alisases
setopt always_to_end     # when completing from middle of word, move cursor to end
setopt complete_in_word  # allow completion from within a word/phrase
setopt correct           # spelling correction for commands
setopt list_ambiguous    # complete as much of a completion until it gets ambiguous.
setopt menu_complete     # complete first choice when entering menu
bindkey -M menuselect '^M' .accept-line
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
kitty + complete setup zsh | source /dev/stdin

# Help function
unalias run-help 2> /dev/null
autoload run-help
HELPDIR=/usr/share/zsh/help/
alias help=run-help

# Set stack-limit to unlimited persistantly
ulimit -s unlimited

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
