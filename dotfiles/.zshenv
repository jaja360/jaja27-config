# Viewers
export EDITOR="vim"
export PDFVIEWER="zathura"
if [ -x "$(command -v delta)" ]; then
  export GIT_PAGER="delta"
else
  export GIT_PAGER="less"
fi

# Misc
export PATH="/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/lib/jvm/default/bin:/usr/lib/rustup/bin:$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.krew/bin"
export OMP_NUM_THREADS=`grep processor /proc/cpuinfo | wc -l`
export SSH_KEY_PATH="$HOME/.ssh/id_ed25519"
export RANGER_LOAD_DEFAULT_RC=false

# Secrets
source "$HOME/.zshenv_secrets"
