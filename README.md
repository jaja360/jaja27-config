# Dépôt contenant des fichiers de configuration

- .gitconfig
- .vimrc
- .bashrc
- .zshrc
- .tmux.conf
- .ssh/config
- Des plugins pour vim
- Un script bash installateur
