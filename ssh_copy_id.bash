#!/usr/bin/env bash

for HOST in `cat ~/.ssh/config | grep "Host " | cut -d' ' -f2`; do
  ssh-copy-id $HOST
done
