#!/usr/bin/env bash

# Requires sudo and git installed
main () {
  echo -e "--- Displaying main menu ---\n"

  sudo pacman -S --needed --noconfirm dialog
  git submodule init
  git submodule update

  dialog --output-fd 1 --checklist "Choose things to install:" 18 50 10 \
    1  "Update all apt packages" on \
    2  "Install system packages" on \
    3  "Install Gnome and graphical apps" on \
    4  "Install Gaming config" on \
    5  "Install programming libraries" on \
    6  "Install zsh" on \
    7  "Install ranger" on \
    8  "Install Latex" on \
    9  "Install python packages" on \
    10 "Install dotfiles" on > choice.txt

  if grep -qw 1 choice.txt ; then
    update
  fi
  if grep -qw 2 choice.txt ; then
    pkg_install
  fi
  if grep -qw 4 choice.txt ; then
    gaming_install
  fi
  if grep -qw 3 choice.txt ; then
    graphical_install
  fi
  if grep -qw 5 choice.txt ; then
    prog_libraries_install
  fi
  if grep -qw 6 choice.txt ; then
    zsh_install
  fi
  if grep -qw 7 choice.txt ; then
    ranger_install
  fi
  if grep -qw 8 choice.txt ; then
    latex_install
  fi
  if grep -qw 9 choice.txt ; then
    python_packages_install
  fi
  if grep -qw 10 choice.txt ; then
    dotfiles_install
  fi

  rm -f choice.txt
}

update () {
  echo -e "\n--- Updating and cleaning system packages ---\n"

  sudo pacman -Syu --noconfirm
  sudo pacman -Sc --noconfirm
}

pkg_install () {
  echo -e "\n--- Installing system packages ---\n"

  # Rust
  sudo pacman -S --needed --noconfirm rustup
  rustup default stable

  # Paru
  sudo pacman -S --needed --noconfirm base-devel
  git clone https://aur.archlinux.org/paru.git
  cd paru
  makepkg -si
  cd ..

  paru -S --needed --noconfirm \
    davmail ansifilter urlview streamrip bvi torguard pandoc-bin \
    aur-auto-vote-git

  sudo systemctl enable aur-auto-vote.timer

  sudo pacman -S --needed --noconfirm \
    git git-delta bat bzip2 curl dos2unix fastfetch \
    kitty-terminfo eza fzf sad graphviz htop btop \
    imagemagick neomutt net-tools openssh wireguard-tools \
    pdftk python ripgrep tree xdotool xclip xxd gnuplot \
    fail2ban flac age ffmpeg openconnect-sso sshfs \
    lsb-release sleuthkit util-linux gvim mono go nodejs \
    npm jdk-openjdk namcap pacman-contrib devtools sysstat \
    procs hyperfine tmux python-libtmux jq yq qrencode websocat entr

  # Docker
  sudo pacman -S --needed --noconfirm \
    docker docker-compose
  sudo systemctl enable --now docker.socket
  sudo groupadd docker
  sudo usermod -aG docker $USER

  # Kubernetes
  sudo pacman -S --needed --noconfirm \
    kubectl krew talosctl fluxcd helm k9s
  kubectl krew update
  kubectl krew install open-svc pv-mounter browse-pvc df-pv

  sudo systemctl enable --now fstrim.timer
}

graphical_install () {
  echo -e "\n--- Installing Gnome and graphical apps ---\n"

  sudo pacman -S --needed --noconfirm \
    gnome gnome-tweaks gnome-browser-connector mattermost-desktop \
    kitty gparted discord zathura zathura-pdf-mupdf \
    xournalpp mpv smplayer bleachbit nextcloud-client firefox \
    mkvtoolnix-gui libreoffice-still hunspell-fr hunspell-en_us

  # Network, Bluetooth, Mouse/Keyboard, Audio, Printer, RGB, Graphics
  sudo pacman -S --needed \
    networkmanager bluez bluez-utils solaar \
    pipewire pipewire-audio pipewire-pulse pipewire-jack pipewire-alsa \
    cups cups-pdf system-config-printer openrgb i2c-tools
    nvidia-open nvidia-settings nvidia-utils lib32-nvidia-utils nvtop

  sudo systemctl enable --now NetworkManager bluetooth cups
  systemctl --user enable --now pipewire pipewire-pulse

  # RGB Control
  sudo touch /etc/modules-load.d/i2c.conf
  sudo sh -c 'echo "i2c-dev" >> /etc/modules-load.d/i2c.conf'
  sudo sh -c 'echo "i2c-piix4" >> /etc/modules-load.d/i2c.conf'

  paru -S --needed --noconfirm \
    zotero-bin exodus teams zoom makemkv signal-desktop
}

gaming_install () {
  echo -e "\n--- Installing gaming libraries and tools ---\n"

  # Wine
  sudo pacman -S --needed --noconfirm \
    wine-staging wine-gecko wine-mono winetricks

  # Libraries
  sudo pacman -S --needed --asdeps --noconfirm \
    giflib lib32-giflib gnutls lib32-gnutls v4l-utils lib32-v4l-utils \
    libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib \
    lib32-alsa-lib sqlite lib32-sqlite libxcomposite lib32-libxcomposite \
    ocl-icd lib32-ocl-icd libva lib32-libva gtk3 lib32-gtk3 \
    gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader \
    lib32-vulkan-icd-loader sdl2 lib32-sdl2

  # Custom Wine/Proton
  paru -S protonup-qt

  # Gaming tools
  sudo pacman -S --needed --noconfirm \
    steam lutris gamemode lib32-gamemode
}

prog_libraries_install () {
  echo -e "\n--- Installing programming libraries and tools ---\n"

  sudo pacman -S --needed --noconfirm \
    astyle cmake doxygen ghc openmp sqlite google-glog \
    gflags boost boost-libs jdk-openjdk swi-prolog \
    gdb gdb-dashboard valgrind pkgconf bear nasm cereal \
    posix posix-software-development posix-c-development \
    posix-user-portability posix-xsi

  paru -S --needed --noconfirm cppman likwid
  sudo cppman -s cppreference.com -m true -o
}

zsh_install () {
  echo -e "\n--- Installing zsh ---\n"

  sudo pacman -S --needed --noconfirm zsh pkgfile
  chsh -s `which zsh`

  sudo pkgfile -u
  sudo systemctl enable --now pkgfile-update.timer

  # oh-my-zsh
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

  # powerlevel10k
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

  # zsh-vi-mode
  git clone https://github.com/jeffreytse/zsh-vi-mode ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-vi-mode

  # zsh-fast-syntax-highlighting
  git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/fast-syntax-highlighting

  # zsh-completions
  git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-completions

  # zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
}

ranger_install () {
  echo -e "\n--- Installing ranger ---\n"

  sudo pacman -S --needed --noconfirm \
    ffmpegthumbnailer highlight odt2txt python-pillow \
    atool w3m poppler mediainfo ranger
  ranger --copy-config=all

  # Plugins
  mkdir -p ~/.config/ranger/plugins/
  git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
  echo "default_linemode devicons" >> $HOME/.config/ranger/rc.conf
}

latex_install () {
  echo -e "\n--- Installing latex ---\n"

  sudo pacman -S --needed --noconfirm \
    texlive texlive-bin texlive-langfrench texlive-langenglish asciidoc
}

python_packages_install () {
  echo -e "\n--- Installing python packages ---\n"

  sudo pacman -S --needed --noconfirm \
    python-flask python-flask-sqlalchemy python-matplotlib python-seaborn \
    python-pipenv python-requests python-beautifulsoup4 python-geopy \
    python-pylint python-pandas python-pandas-datareader python-xlrd \
    python-numpy python-scipy python-pygments yapf flake8 jupyterlab ipython

  paru -S --needed --noconfirm \
    python-bokeh python-inkscape-figures python-folium python-nasdaq-data-link python-glob2
}

dotfiles_install () {
  echo -e "\n--- Copying dotfiles ---\n"

  # Install themes
  # Use grub-customizer to apply grub theme
  paru -S --needed --noconfirm \
    catppuccin-gtk-theme-mocha catppuccin-cursors-mocha \
    btop-theme-catppuccin catppuccin-mocha-grub-theme-git \
    ttf-firacode-nerd

  # User systemd services
  systemctl enable --user davmail.service

  ln -s /usr/share/gdb-dashboard/.gdbinit ~/.gdbinit
  cp -a ./dotfiles/. ~
  bat cache --build
  age -d -o ~/.zshenv_secrets ./dotfiles/.zshenv_secrets.age
  age -d -o ~/.ssh/config ./dotfiles/.ssh/config.age
  rm -f ~/.zshenv_secrets.age
  rm -f ~/.ssh/config.age
}

if [ "$0" = "$BASH_SOURCE" ] ; then
  main
fi
