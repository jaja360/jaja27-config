# Install Wine
paru -S --needed --noconfirm wine-staging winetricks

# Make CD Drive detectable by Wine
sudo mkdir -p /mnt/drive0
echo -e '\n# sr0 for EAC\n/dev/sr0 /mnt/drive0 auto ro,user,noauto,unhide 0 0' | sudo tee -a /etc/fstab > /dev/null
sudo gpasswd -a jgareau optical

# Make current session consider new groups
# Reboot necessary ?
exec su -l $USER

# Set new Wine prefix
mkdir ~/.eac-prefix
export WINEPREFIX=/home/jgareau/.eac-prefix
export WINEARCH=win32

# Add drive in wine config
winecfg

# Install Dependencies
winetricks dotnet20
winetricks dotnet40
winetricks vcrun2008

# Install EAC
# Download on https://www.exactaudiocopy.de/en/index.php/resources/download/
wine eac-1.8.exe
cp ./Exact Audio Copy.desktop ~/Bureau

# Follow configuration: https://redacted.ch/wiki.php?action=article&id=59
