loadkeys cf
setfont ter-i24n

# Sanity checks
cat /sys/firmware/efi/fw_platform_size # must show 64
ip link # check if internet is ok
timedatectl # verify time

# Partition disk
fdisk -l
fdisk /dev/...
mkfs.fat -F 32 /dev/...
mkfs.ext4 /dev/...

mount /dev/... /mnt
mount --mkdir /dev/... /mnt/boot

# Install base system
pacstrap -K /mnt base linux linux-firmware amd-ucode \
  nano gvim man-db man-pages texinfo terminus-font reflector \
  networkmanager # optionnel
genfstab -L /mnt >> /mnt/etc/fstab

arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Canada/Eastern /etc/localtime
hwclock --systohc

echo "nom" > /etc/hostname

# Copy system configuration files
cp -dR jaja27-config/arch/etc/. /etc
locale-gen

# Install boot-loader
pacman -S grub efibootmgr os-prober grub-customizer \
  edk2-shell memtest86+-efi fwupd udisks2
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# Activate some systemd units
systemctl enable \
  NetworkManager # ou systemd-networkd systemd-resolved
  systemd-timesyncd reflector.timer fwupd-refresh.timer

# Create a user
passwd # Change root passwd
useradd -m -G "wheel,users" jgareau # Create a user named jgareau
passwd jgareau # Change passwd of new user

exit # exit the chroot environment
umount -R /mnt
reboot

# Post-install
journalctl -k --grep='microcode:' # Check that amd-uboot has been loaded
networkctl # Check state of network interfaces
ip link set [name_interface] up # Activate the given interface
# Make sure the interface name in /etc/systemd/network/20-wired.network is correct
